#include <iostream>
#include <conio.h>
#include <string>

// Card struct and two enum

enum Rank
{
    ACE = 14,
    KING = 13,
    QUEEN = 12,
    JACK = 11,
    TEN = 10,
    NINE = 9,
    EIGHT = 8,
    SEVEN = 7,
    SIX = 6,
    FIVE = 5,
    FOR = 4,
    THREE = 3,
    TWO = 2
};

enum Suit
{
    HEARTS,
    DIAMONDS,
    CLUBS,
    SPADES
};

struct Card
{
    Rank Number;
    Suit Type;
};

int main()
{

    _getch();
    return 0;
}
